# Tinyrpg Outline

## Requirements for completion:
1. Working camera that follows player and has boundary support
2. Overworld must include:
    * Location changing
    * Simple support for scripted events (warps, conveyers, etc)
    * Collisions of course
    * NPCs and other entities
3. Menus (however simplistic) must be implemented to be considered complete