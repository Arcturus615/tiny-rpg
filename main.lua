require('src.utils')
addlibrarypath('src/lib')
addclibrarypath(love.filesystem.getRealDirectory('main.lua'))
require('Moan')
require('console.console')
_G.class = require('class')

_G.SAVES= love.filesystem.getSaveDirectory()..'tinyrpg/'
_G.COMMON = 'src/common/'
_G.TITLE = 'Tiny RPG'
_G.GAMEWIDTH = 240
_G.GAMEHEIGHT = 160
_G.UP = -1
_G.DOWN = 1
_G.LEFT = -1
_G.RIGHT = 1

do
	local gamestate
	function love.load(arguments)
		local defaultparams = { debug = false }
		local parameters = parseparameters(arguments, defaultparams)
		love.math.random(); love.math.random(); love.math.random()

		love.graphics.setDefaultFilter('nearest','nearest')
		_G.config = require('src.configuration').init()
		gamestate = require('src.gamestate')(parameters.debug)
		gamestate:start()
	end

	function love.update(dt)
		gamestate:update(dt)
		require('lovebird').update()
	end

	function love.draw()
		gamestate:draw()
	end

	function love.keypressed(...)
		gamestate:returnCurrentState():keypressed(...)
	end

	function love.keyreleased(...)
		gamestate:returnCurrentState():keyreleased(...)
	end

	function love.mousemoved(...)
		gamestate:returnCurrentState():mousemoved(...)
	end

	function love.mousepressed(...)
		gamestate:returnCurrentState():mousepressed(...)
	end

	function love.mousereleased(...)
		gamestate:returnCurrentState():mousereleased(...)
	end

	function love.textinput(...)
		gamestate:returnCurrentState():textinput(...)
	end

	function love.wheelmoved(...)
		gamestate:returnCurrentState():wheelmoved(...)
	end
end
