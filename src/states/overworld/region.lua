local path = 'src/states/overworld/'
local tile = require('src.common.graphics.tile')
local regions = 'src/assets/regions/'
local color = require('color')
local graphic = require('graphic')

local Region = class('Region', require('src.common.gameobject'))

local _p = setmetatable({}, {__mode = 'k'})

do
  local function _assignTilemap(self, atlasPath, tw, th)
    self:cache('store', 'image', 'missing')
    self:cache('store', 'image', atlasPath)
    local _image = self:cache('retrieve', 'image', atlasPath)
    local _quads = {}
    local spacing = 2
    local margin = 1
    local iw = _image:getWidth()
    local ih = _image:getHeight()
    local tilesWide = math.floor(iw/tw) -- Temporary hack (l year later, still here xD)
    local tilesHigh = math.floor(ih/th)
    local count

    for h=1, tilesHigh do
      for w=1, tilesWide do
        local quad = love.graphics.newQuad(
          (tw) * (w-1) + margin + ( (w-1) > 0 and spacing or 0 ),
          (th) * (h-1) + margin + ( (h-1) > 0 and spacing or 0 ),
          tw, th, iw, ih)
        table.insert(_quads, quad)
        count = (count or 0) + 1
      end
    end
    return {image=_image, quads=_quads}
  end

  local function _processLayers(layers, tw, th)
    local _layers = {}

    for z, layer in ipairs(layers) do
      local y = 1
      local x = 1
      for y, row in ipairs(layer) do
        if not _layers[z] then _layers[z] = {} end
        for x, data in ipairs(row) do
          if not _layers[z][y] then _layers[z][y] = {} end
            local _tile = tile( ((x - 1) * tw), ((y - 1) * th), tw, th, data)
            table.insert(_layers[z][y], _tile)
        end
      end
    end

    return _layers
  end

  local function _spawnEntities(entities, tw, th)
    local _entities = {}
    for _, entity in ipairs(entities) do
      local entdata = require(path..'entities/npcs')[entity.id]
      local _entity = require(path..'entity')(
        entdata,
        entity.gridpos.x * tw,
        entity.gridpos.y * th,
        entity.dir.x,
        entity.dir.y
      )

      if entity.attributes then
        for attribute, value in pairs(entity.attributes) do
          _entity:addAttribute(attribute, value)
        end
      end

      if entity.components then
        for _, component in ipairs(entity.components) do
          _entity:addComponent(component)
        end
      end

      _entity:setGridPos(entity.gridPosX, entity.gridPosY)
      table.insert( _entities, _entity )
    end

    return _entities
  end

  function Region:__init(regioncoords)
    local data = require(regions..'region_'..regioncoords)
    self:message('Loading '..data.name..' into memory')
    _p[self] = {}
    _p[self].__coords = regioncoords
    _p[self].atlas = _assignTilemap(
      self,
      data.atlasPath,
      data.tilewidth,
      data.tileheight,
      data.atlHeight,
      data.atlWidth)
    _p[self].entities = _spawnEntities(data.entities, data.tilewidth, data.tileheight)
    _p[self].layers = _processLayers(data.layers, data.tilewidth, data.tileheight)
    _p[self].bordering = data.bordering
    _p[self].name = data.name
    _p[self].width = data.width
    _p[self].height = data.height
    _p[self].tilewidth = data.tilewidth
    _p[self].tileheight = data.tileheight
  end
end

function Region:update(dt, map, camera)
  for _, entity in ipairs(_p[self].entities) do
    entity:update(dt, map, camera)
  end
end

function Region:draw()
  for _, __layer in ipairs(_p[self].layers) do
    for _, __row in ipairs(__layer) do
      for _, __tile in ipairs(__row) do
        __tile:draw(
          _p[self].atlas.image,
          _p[self].atlas.quads[__tile.qid],
          _p[self].xoffset,
          _p[self].yoffset)
      end
    end
  end

  for _, entity in ipairs(_p[self].entities) do
    entity:draw(_p[self].xoffset, _p[self].yoffset)
  end
end

function Region:updateDebug()
  self:addDebugInfo('Region: '.. tostring(_p[self].name) )
end

function Region:drawDebug()
  local ea, we, no, so = self:getPreloadBounds()
  local realw = (_p[self].width * _p[self].tilewidth)
  local realh = (_p[self].height * _p[self].tileheight)

  graphic.rectLine(
    color.YELLOW,
    (_p[self].xoffset or 0),
    (_p[self].yoffset or 0),
    realw,
    realh,
    1
  )

  -- Preload lines (use this to determine whether preloading is working)
  graphic.line( color.PURPLE, 0, no, realw, no, 1 ) --north
  graphic.line( color.PURPLE, 0, so, realw, so, 1 ) --south
  graphic.line( color.PURPLE, ea, 0, ea, realh, 1 ) --east
  graphic.line( color.PURPLE, we, 0, we, realh, 1 ) --west


  for _, entity in ipairs(_p[self].entities) do
    entity:drawDebug(_p[self].xoffset, _p[self].yoffset)
  end
end

function Region:countEntities()
  local i = 0
  for _, v in ipairs(_p[self].entities) do
    i = i + 1
  end
  return i
end

function Region:getDimensions()
  return
    _p[self].width,
    _p[self].height,
    _p[self].tilewidth,
    _p[self].tileheight,
    (_p[self].width * _p[self].tilewidth),
    (_p[self].height * _p[self].tileheight)
end

function Region:getPreloadBounds()
  local rw, rh, tw, th, realw, realh = self:getDimensions()
  return
    (realw / 3),
    (realw - (realw / 3)),
    (realh / 3),
    (realh - (realh / 3))
end

function Region:getCoords()
  return _p[self].__coords:split('-')
end

function Region:getRawCoords()
  return _p[self].__coords.x, _p[self].__coords.y
end

function Region:getSetting()
  return (_p[self].__setting or 'plains')
end

function Region:getEntities()
  return _p[self].entities
end

return Region