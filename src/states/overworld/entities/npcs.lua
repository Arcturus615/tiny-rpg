return {
    debugNPC1 = {
        name = 'Mysterious old man',
        id = 'debugNPC1',
        imgPath = false,
        animation = false,
        movement = 'pace',
        speed = 20
    },
    debugNPC2 = {
        name = 'Cheerful young woman',
        id = 'debugNPC2',
        imgPath = false,
        animation = false,
        movement = false,
        speed = 30
    }
}