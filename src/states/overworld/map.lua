local path = 'src/states/overworld/'
local regions = 'src/assets/regions/'
local graphic = require('graphic')
local color = require('color')

local Map = class('Map', require('src.common.gameobject'))

function Map:__init(mapx, mapy)
  self:message('Starting map initialization')
  if not mapx or not mapy
  and mapx > 0 and mapy > 0
  then
    self:error('Didnt recieve map size', true)
  end

  self.__width = mapx
  self.__height = mapy

  self.player = require(path..'entity')(path..'entities/player', 5 * 16, 5 * 16)
  self.player:addComponent('interaction_ability')
  self.player:addAttribute('player')
  self.player:setName('Drew')

  self.player:scripts('add', {
    name = 'testScript',
    __interval = 1,
    script = function ()
    end
  })

  self.__plrLocX = 1
  self.__plrLocY = 1

  self.__regions = {}
  for y=0, (mapy) do
    for x=0, (mapx) do
      if not self.__regions[y] then self.__regions[y] = {} end
      if love.filesystem.getInfo(regions..'region_'..tostring(x)..'-'..tostring(y)..'.lua') then
        self.__regions[y][x] = require(path..'region')(tostring(x)..'-'..tostring(y))
      else
        self.__regions[y][x] = false
      end
    end
  end
end

local function __setCameraBounds(c, tw, th, rw, rh, realw, realh)
  local gw = GAMEWIDTH
  local gh = GAMEHEIGHT

  c:setBoundary(
    0,0,
    (
      gw < realw
      and (c.scaleX * (realw - gw))
      or  (c.scaleX * (gw - realw))
    ),
    (
      gh < realh
      and (c.scaleY * (realh - gh))
      or  (c.scaleY * (gh - realh))
    )
  )
end

local function __setCameraPosition(c, p)
  c:setPosition(
    (p.box.x + (p.box.w/2) ) * c.scaleX - love.graphics.getWidth()/2,
    (p.box.y + (p.box.h/2) ) * c.scaleY - love.graphics.getHeight()/2
    )
end

local function __checkPlayerReachedBoundary(self, player, rw, rh, tw, th, realw, realh)
  local x, y, w, h = player.box:getDimensions()
  local dx = player.dir.x
  local dy = player.dir.y
  local rx, ry = self:getPlayerLocation()

  if y <= 0 and dy == UP then
    if self:load(tonumber(rx), tonumber(ry - 1), 'north') then
      rw, rh, tw, th, realw, realh = self:returnCurrentRegion():getDimensions()
      player.pos.y = realh - th
      player.box.y = realh - th
      return true
    else
      player.pos.y = 0
      player.box.y = 0
      return false
    end

  elseif (y+h) >= realh and dy == DOWN then
    if self:load(tonumber(rx), tonumber(ry + 1), 'south') then
      player.pos.y = 0
      player.box.y = 0
      return true
    else
      player.pos.y = realh - th
      player.box.y = realh - th
      return false
    end

  elseif (x+w) >= realw and dx == RIGHT then
    if self:load(tonumber(rx + 1), tonumber(ry), 'east') then
      player.pos.x = 0
      player.box.x = 0
      return true
    else
      player.pos.x = realw - tw
      player.box.x = realw - tw
      return false
    end

  elseif x <= 0 and dx == LEFT then
    if self:load(tonumber(rx - 1), tonumber(ry), 'west') then
      rw, rh, tw, th, realw, realh = self:returnCurrentRegion():getDimensions()
      player.pos.x = realw - tw
      player.box.x = realw - tw
      return true
    else
      player.pos.x = 0
      player.box.x = 0
      return false
    end
  end
end

local function __getOppositeCardinal(cardinal)
  local opposite = ''
  if cardinal:match('north') then opposite = opposite .. 'south' end
  if cardinal:match('south') then opposite = opposite .. 'north' end
  if cardinal:match('west') then opposite = opposite .. 'east' end
  if cardinal:match('east') then opposite = opposite .. 'west' end
  return opposite
end

function Map:load(x, y)
  assert(type(x) == 'number')
  assert(type(y) == 'number')

  if not self:checkRegionLoaded(x, y) then return false end

  self:setPlayerLocation(x, y)
  self:returnCurrentRegion().xoffset = false
  self:returnCurrentRegion().yoffset = false
  return true
end

function Map:update(dt, camera)
  self.__regions[self.__plrLocY][self.__plrLocX]:update(dt, self, camera)
  self.player:update(dt, self, camera)

  __checkPlayerReachedBoundary(self, self.player, self:returnCurrentRegion():getDimensions())
  __setCameraBounds(camera, self:returnCurrentRegion():getDimensions())
  __setCameraPosition(camera, self.player)
end

function Map:draw()
  self.__regions[self.__plrLocY][self.__plrLocX]:draw()
  self.player:draw()
end

function Map:updateDebug()
  local x, y = self:getPlayerLocation()
  local w, h = self:returnCurrentRegion():getDimensions()
  self.addDebugData('Region', '('..x..','..y..') ('..tostring(self:returnCurrentRegion())..')')
  self.addDebugData('Region Size', '('..w..','..h..')')
  self.addDebugData('Entity Count', self:getEntityCount())
end

function Map:drawDebug()
  self:returnCurrentRegion():drawDebug()
  self.player:drawDebug()
end

function Map:getEntityCount()
  return 1 + self:returnCurrentRegion():countEntities()
end

function Map:getPlayer()
  return self.player
end

function Map:getMapSize()
  return self.__width, self.__height
end

function Map:returnCurrentRegion()
  local function locationInvalid()
    self:error('Players current region either undefined or invalid')
    return false
  end

  return self.__regions[ (self.__plrLocY or locationInvalid(self)) ][ (self.__plrLocX or locationInvalid(self)) ]
end

function Map:setPlayerLocation(x, y)
  self.__plrLocX = (x or self.__plrLocX)
  self.__plrLocY = (y or self.__plrLocY)
end

function Map:getPlayerLocation()
  return self.__plrLocX, self.__plrLocY
end

function Map:checkRegionLoaded(x, y)
  if self.__regions[y] then
    if self.__regions[y][x] then
      return true
    end
  end

  return false
end

function Map:warpPlayer(x, y)
  if self:checkRegionLoaded(x, y) then
    self:message('Warping player to region ('..x..','..y..')')
    self:setPlayerLocation(x, y)
    if self:returnCurrentRegion().warppoint then
      local _x, _y = unpack(self.returnCurrentRegion().warppoint)
      self.player.setPosition(_x, _y)
    else
      local _,_,_,_, _x, _y = self:returnCurrentRegion():getDimensions()
      self.player:setPosition( math.floor(_x/2), math.floor(_y/2) )
    end
  else
    return false
  end
end

return Map
