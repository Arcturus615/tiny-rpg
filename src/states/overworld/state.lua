local path = 'src/states/overworld/'
local OverworldState = class('OverworldState', require('src.common.state'))

function OverworldState:__init()
  self.name = 'main'
  self.camera = require('camera').init()
  self.camera:setCanvas(love.graphics.newCanvas(GAMEWIDTH,GAMEHEIGHT))
  
  self.map = require(path..'map')(15, 10)
  self.map:load(1, 1)
  self.time = 0
  self:message('Loaded new overworld into memory')
end

function OverworldState:keypressed(key)
  if config.keyboard:check('quit', key) then love.event.push('quit')
  elseif config.keyboard:check('interact', key) then self:checkPlayerInteraction()
  elseif config.keyboard:check('pause', key) then EngineState:load('worldmap', self.map)
  elseif config.keyboard:check('editor', key) then EngineState:load('editor')
  elseif config.keyboard:check('debug', key) then EngineState:toggleDebugmode()
  elseif config.keyboard:check('console', key) then console.Show()
  end
end

function OverworldState:update(dt)
  local w = love.graphics.getWidth()
  local h = love.graphics.getHeight()
  self.camera:setScale(w/GAMEWIDTH, h/GAMEHEIGHT)

  self.time = self.time + dt
  self.map:update(dt, self.camera)
end

function OverworldState:draw()
  self.camera:set()
    self.map:draw()
  self.camera:unset()
end

function OverworldState:updateDebug()
  self.map:updateDebug()
end

function OverworldState:drawDebug()
  self.camera:set()
    self.map:drawDebug()
  self.camera:unset()
end

function OverworldState:checkPlayerInteraction()
  local region = self.map:returnCurrentRegion()

  if self.map:getPlayer().interact then
    for _, entity in ipairs(region:getEntities()) do
      self.map:getPlayer():interact(entity)
    end
  end
end

function OverworldState:getName()
  return (self.name)
end

return OverworldState