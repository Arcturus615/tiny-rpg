local path = 'src/states/overworld/'
local vector = require('vector')
local hitbox = require('hitbox')
local color = require('color')
local graphic = require('graphic')

local Entity = class('OverworldEntity', require('src.common.gameobject'))

local _p = setmetatable( {}, {__mode='k'} )

function Entity:__init(entityData, x, y, dx, dy)
  local data
  if type(entityData) == 'string' then
    data = require(entityData)
  elseif type(entityData) == 'table' then
    data = entityData
  else
    self:error('entity', 'Invalid NPC data received, skipping entity.')
    return false
  end

  _p[self] = {}
  _p[self].__attributes = {}
  _p[self].__components = {}
  self:setName(data.name)
  self:setID(data.id, true)

  self.speed = data.speed
  self.dir = vector( (dx or 0), (dy or 1) )
  self.vel = vector()
  self.pos = vector( (x or 1), (y or 1) )
  self.box = hitbox()
  self.gridpos = vector()
  self.runspeed = data.runspeed or 1.3

  _p[self].sprite = (
    data.animation
      and require('src.common.graphics.sprite')(data.imgPath, 'src/assets/animations/'..data.animation)
      or require('src.common.graphics.sprite')(data.imgPath)
    )
  _p[self].movement = (
    data.movement
      and require(path..'logic/movement/'..data.movement)(self)
      or require(path..'logic/movement/idle')(self)
    )

  _p[self].__stats = data.stats or {
    health = 10,
    magic = 5,
    attack = 1,
    defense = 1,
    intellect = 1,

    happyness = 10,
    depression = 0,
    hatred = 0,
    love = 5
  }

  self:message('Loaded entity <'..data.id..'>')
end

function Entity:update(dt, map, camera)
  assert(dt, 'Entity: delta not passed!')
  local action = _p[self].movement:update(dt, self) or 'idle'
  _p[self].sprite:update(dt, self.dir, action)

  local animation = _p[self].sprite.animation
  self.vel = self.vel * dt
  self.pos = self.pos + self.vel

  self.box.x = self.pos.x
  self.box.y = self.pos.y
  self.box.w = (
    animation
      and (animation.current.width or animation.defaultwidth)
      or _p[self].sprite.image:getWidth()
    )
  self.box.h = (
    animation
      and (animation.current.height or animation.defaultheight)
      or _p[self].sprite.image:getHeight())

  self:updateComponents(dt, self, camera)
  self:updateScripts(dt, map, camera)
end

function Entity:draw(xoffset, yoffset)
  _p[self].sprite:draw(
    self.pos.x + (xoffset or 0),
    self.pos.y + (yoffset or 0),
    self.dir)
  for _, component in ipairs(_p[self].__components) do
    component:draw(_p[self], xoffset, yoffset)
  end
end

function Entity:drawDebug(xoffset, yoffset)
  graphic.rectLine(
    color.RED,
    self.box.x + (xoffset or 0),
    self.box.y + (yoffset or 0),
    self.box.w,
    self.box.h,
    1
  )
  for _, component in ipairs(_p[self].__components) do
    component:drawDebug(_p[self], xoffset, yoffset)
  end
end

function Entity:getDirectionFacing()
  local facing = ''
  if self.dir.x > 0 then facing = 'left'
  elseif self.dir.x < 0 then facing = 'right'
  elseif self.dir.y > 0 then facing = 'down'
  elseif self.dir.y < 0 then facing = 'up'
  end

  return facing .. ' (' ..tostring(self.dir) .. ')'
end

function Entity:getDirection()
  return self.dir.x, self.dir.x
end

function Entity:getPosition()
  return self.pos.x, self.pos.y
end

function Entity:getGridPos()
  return self.gridpos.x, self.gridpos.y
end

function Entity:getVelocity()
  return self.vel.x, self.vel.y
end

function Entity:getSpeed()
  return self.speed
end

function Entity:getHitBox()
  return self.box.x, self.box.y, self.box.w, self.box.h
end

function Entity:setDirectionFacing(facing)
  if facing:match('up') then self.dir.y = -1 end
  if facing:match('down') then self.dir.y = 1 end
  if facing:match('left') then self.dir.x = -1 end
  if facing:match('right') then self.dir.x = 1 end
end

function Entity:setDirection(dx, dy)
  self.dir.x = ( type(dx) == 'number' and dx or self.dir.x )
  self.dir.y = ( type(dy) == 'number' and dy or self.dir.y )
end

function Entity:setGridPos(x, y)
  if not self.gridpos then self.gridpos = vector() end
  self.gridpos.x = ( type(x) == 'number' and x or self.gridpos.x )
  self.gridpos.y = ( type(y) == 'number' and y or self.gridpos.y )
end

function Entity:setPosition(x, y)
  self.pos.x = ( type(x) == 'number' and x or self.pos.x )
  self.pos.y = ( type(y) == 'number' and y or self.pos.y )
end

function Entity:setVelocity(x, y)
  self.vel.x = ( type(x) == 'number' and x or self.vel.x )
  self.vel.y = ( type(y) == 'number' and y or self.vel.y )
end

function Entity:setSpeed(spd)
  self.speed = (type(spd) == 'number' and spd or self.speed)
end

function Entity:modifyStat(stat, amount)
  if not _p[self].__stats[stat] then
    self:error('Stat <'..tostring(stat)..'> is invalid')
    return false
  end
  _p[self].__stats[stat] = _p[self].__stats[stat] + amount
  return true
end

return Entity