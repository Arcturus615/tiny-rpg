local vector = require('vector')

local Pace = class('PaveAI', require('src/states/overworld/logic/movement/movement'))

function Pace:__init(entity)
  self.__delta = 0
  entity.dir = vector(1, 0)
end

function Pace:update(dt, entity)
  entity.vel.x = (entity.speed or 10) * entity.dir.x
  if self.__delta >= 3 then
    self.__delta = 0
    entity.dir.x = -(entity.dir.x)
  end
  self.__delta = self.__delta + dt
  return 'walk'
end

return Pace