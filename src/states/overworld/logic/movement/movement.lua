local Movement = class('Movement', require('src.common.gameobject'))

Movement.__tostring = function(self) return self.__name end

function Movement:__init(entity)
  self.__name = entity.__name
end

function Movement:update(entity, dt)
  return 'idle'
end

return Movement