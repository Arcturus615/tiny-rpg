local vector = require('vector')

local Input = class('PlayerInput', require('src/states/overworld/logic/movement/movement'))

function Input:up(entity)
  if entity.vel.y <= 0 then
    entity.vel.y = -(entity.speed * (self.__running and entity.runspeed or 1))
    entity.dir = vector(0, -1)
    return 'walk'
  else
    return self:halt(entity)
  end
end

function Input:down(entity)
  if entity.vel.y >= 0 then
    entity.vel.y = (entity.speed * (self.__running and entity.runspeed or 1))
    entity.dir = vector(0, 1)
    return 'walk'
  else
    return self:halt(entity)
  end
end

function Input:left(entity)
  if entity.vel.x <= 0 then
    entity.vel.x = -(entity.speed * (self.__running and entity.runspeed or 1))
    entity.dir = vector(-1, 0)
    return 'walk'
  else
    return self:halt(entity)
  end
end

function Input:right(entity)
  if entity.vel.x >= 0 then
    entity.vel.x = (entity.speed * (self.__running and entity.runspeed or 1))
    entity.dir = vector(1, 0)
    return 'walk'
  else
    return self:halt(entity)
  end
end

function Input:halt(entity)
  entity.vel.y = 0
  entity.vel.x = 0
  return 'idle'
end

function Input:update(dt, entity)
  local action
  local down = love.keyboard.isDown

  --if not down('any') then action = self:halt(entity) end
  
  if config.keyboard:check('run') then self.__running = true
  else self.__running = false
  end

  if config.keyboard:check('up') then action = self:up(entity)
  elseif config.keyboard:check('down') then action = self:down(entity)
  elseif config.keyboard:check('left') then action = self:left(entity)
  elseif config.keyboard:check('right') then action = self:right(entity)
  end

  return action
end

return Input