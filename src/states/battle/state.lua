-- General Flow of Battle:
-- Every frame update, the state object will check whether or
-- not each party has decided on a course of action. One both
-- have, the following occurs:
--
-- The state object grabs the stat spread of each parties spd
-- attribute. These will be in the form of tables, with the 
-- spd values themselves as keyed tables containing member
-- indexes. The state object will then determine which party members with
-- the same value, out of both parties,  have the advantage in that
-- spd bracket. It should be noted that the state will *include*
-- brackets with very similar speeds, up to a 10 point difference
-- (out of 999 spd stat). For advantage, if there is no special
-- effect in play forcing advantage for a specifc party member, then
-- the order is randomized. This then continues down the spd brackets
-- in the same fashion.

local State = {}

function State:load()
end

function State:update(dt)
end

function State:draw()
end

return State
