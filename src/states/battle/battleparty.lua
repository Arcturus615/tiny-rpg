local BattleEntity = require('src.states.battle.battleentity')
local BattleParty = class('BattleParty', require('src.common.gameobject'))

local _members = setmetatable({}, {__mode='k'})

-- Initialize the party using the tables provided to populate the battle parties
function BattleParty:__init(_party_data)
    -- Create the private local table
    _members[self] = {}

    -- Test the recieved party data and confirm that it is valid
    if not self:test('valid_party', _party_data) then
        error('Error on '.. party ..'Party definition malformed!', true)
    end

    -- Instantiate the members using the data provided
    for i, member in ipairs(_party_data) do
        table.insert(_members[self], BattleEntity(member))
    end
end

function BattleParty:update()
    -- Simply run the standard update. This is simply for sprite updates
    -- and the like. Battle logic will be kept seperate for simplicity
    for i, _member in ipairs(_members[self]) do
        _member:update()
    end
end

function BattleParty:draw()
    for i, _member in ipairs(_members[self]) do
        _member:update()
    end
end

function BattleParty:getMemberAction(_member)
    -- TODO: Refactor this function to instead provide
    -- the member entity with the FULL stat spread
    -- of it's fellow members. This is to prevent
    -- coupling code any further than it needs to be.
    -- This function should be strictly I/O

    -- Check whether or not the specified member has
    -- decided on a course of action. Defaults to false
    -- to prevent popoluating nil and to account for
    -- lack of player action. Provides self in case the
    -- AI that member uses needs to account for the
    -- condition of it's party members in it's decision
    -- making process.
    return _member:getAction(self) or false
end


function BattleParty:getPartyCount()
    return table.length(_members[self])
end

function BattleParty:getPartySpds()
    -- TRIVIAL TODO: Convert this into a function that returns the 
    -- full party spread of a *specified* stat. Not just
    -- speed. This will make AI coding easier. There shoud be a 
    -- special instruction set for HP/MP which returns the current
    -- value, as well as the total value of the stat in question.

    -- In order to determine the flow of battle, we'll need
    -- to get the actual speed of every party member. To do
    -- so, we return a table which is uses the actual member
    -- speeds as an index which then contains the members own
    -- internal party indexes. By having the party members own
    -- speeds as the indexes, we can assign members with the same
    -- speed to the same index. This allows the calculation we 
    -- will be doing later along this path to to be far simpler.

    -- First, get the number of members in the party and assign
    -- it to a variable. Then, initialize an empty table, which
    -- will later be the table that this method returns. Finally,
    -- create an empty table, which we will use to determine if a
    -- member has been indexed yet or not.
    local _member_count = self:getPartyCount()
    local _spd_index = {}
    local _indexed_members = {}

    -- Next up, we start a while loop that iterates as long as the 
    -- number of indexed members is less than the number of members
    -- in the party in total. Then, we run a for loop, in which the
    -- index is given as i, and the member is assigned to the variable
    -- "member". For each index in the for loop, we check the indexed
    -- members table we created prior for an a value matching the
    -- current members index. If it is not indexed, we proceed to assign
    -- the members speed (after effects) to a variable. We then ensure
    -- _spd_table has that speed as an index, and set it to a table.
    -- Following this, we add that members own internal party index,
    -- to the new sub-table we just created. Finally, we add that
    -- members index to the _indexed_members table.
    while table.length(_indexed_members) < _member_count do
        for i, _member in ipairs(_members[self]) do
            if not table.icontains(_indexed_members, i) then
                local _mem_spd = _member:getStat('spd', self:getEffects('spd'))
                _spd_index[_mem_spd] = {}
                table.insert(_spd_index[_mem_spd], i)
                table.insert(_indexed_members, i)
            end
        end
    end

    -- We then return the newly generated index
    return _spd_index
end

function BattleParty:getMemberStat(_member_index, _stat)
    -- Assert that the party member being referenced even exists, and that the stat
    -- being requested is valid
    local _passed_test, _fail_message = self:test('is_valid_stat', _stat)
    assert(_passed_test, _fail_message)
    assert(_members[_member_index], 'Requested invalid member index')

    -- Default to false if the stat is not present on that entity
    _members[self]:getStat(_stat, self:getEffects()) or false
end

function BattleParty:getEffects(_stat)
    -- For now, simply return false. This will be one of the
    -- possible values that this function will return. The goal
    -- of this function, however, will be to return any active
    -- effects that afflicting the specific stat referenced. On
    -- the whole party. NOTE: Each member has there won effects
    -- as well.
    return false
end
