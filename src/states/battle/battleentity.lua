local BattleEntity = class('BattleEntity', require('src.common.GameObject'))

function BattleEntity:__init(_entity_data)
    -- Ensure that the entity data that was returned is valid.
    if not self:test('is_valid_entity', _entity_data) then
        self:error('Entity definition ' .. _entity_data .. 'is malformed!', true)
    end
end

function BattleEntity:update()
end

function BattleEntity:draw()
end