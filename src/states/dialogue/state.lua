local DialogueState = class('DialogueState', require('src.common.state'))

local function __checkIfFinished()
  if #Moan.allMsgs < 1 then
    Moan.clearMessages()
    EngineState:load('overworld')
  end
end

function DialogueState:load()
  Moan.selectButton = config.keyboard:getConfig('interact')
end

function DialogueState:keypressed(key)
  Moan.keypressed(key)
end

function DialogueState:update(dt)
  __checkIfFinished()
  Moan.update(dt)
end

function DialogueState:draw()
  EngineState:drawState('overworld')
  Moan.draw()
end

return DialogueState