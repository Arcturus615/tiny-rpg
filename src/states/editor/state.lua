local State = { __name = 'Map Editor'}

function State.init()
  return setmetatable({}, State)
end

function State:load()
  function love.keypressed(key)
    if key == 'f11' then
      EngineState:load('overworld')
    end
  end
end

function State:update()
end

function State:draw()
end

return State