-- World Map class
-- 
-- Load the current game map into a table
-- and display it. The World Map can also
-- be used to warp to other regions, and
-- will also prevent the user from warping
-- to a region that is yet to be implemented

local graphic = require('graphic')
local color = require('color')

local WorldMap = class('WorldMapState', require('src.common.state'))

local function __determineRegionFill(x, y, px, py, map)
    -- Determines what color the the world map will use for the specified region
	-- Yellow:	Current player location
	-- Green:	Valid region
	-- Red:		Empty region
	if x == px and y == py then return color.YELLOW end
    return tern(map:checkRegionLoaded(x, y), color.GREEN, color.RED)
end

local function __determineRegionOutline(x, y, hx, hy)
    -- Determine the outline of the region specified with 'x' and 'y'
	-- To do this, it compares those value against 'hx' and 'hy'
	-- x/y:		Region being processed
	-- hx/hy:	Current cursor location
	if x == hx and y == hy then
        return color.BLUE
    else
        return color.WHITE
    end
end

function WorldMap:load(map)
    -- Begin processing the specified map, by running the map table through a
	-- table hash.
	
	-- Start by determining whether or not a map was passed, and exit if not.
	-- Then, get the map dimensions, and current player coordinates
	if not map then self:error('No map passed to init! Exiting...', true) end
    self.map = map
    local sx, sy = map:getMapSize()
    local px, py = map:getPlayerLocation()
	
	-- Instead of grabbing the player coordinates every frame, store them
	-- Then, use those coordinates to determine the current highlighted region
	-- NOTE: The "+1" here is to account for Luas method of handling indexs
	--   (Lua starts at 1, rather than 0)
    self._plr = {x=px, y=py}
    self._highlighted = {x=px+1, y=py+1}
    self._regions = nil
    self._regions = {}

    for y=1, (sy+1) do
        for x=1, (sx+1) do
            if not self._regions[y] then self._regions[y] = {} end
            table.insert(
                self._regions[y],
				-- Region color is saved here for use in drawing
                __determineRegionFill(x-1, y-1, self._plr.x, self._plr.y, map)
            )
        end
    end

end

function WorldMap:update(dt)
	-- Update the WorldMap dimensions in case of a window resize
    self._x = love.graphics.getWidth() / 10
    self._y = love.graphics.getHeight() / 10
    self._w = love.graphics.getWidth() - (self._x * 2)
    self._h = love.graphics.getHeight() - (self._y * 2)
end

function WorldMap:keypressed(key)
	-- Process keys pressed while this State is loaded
    if config.keyboard:check('pause', key) then EngineState:load('overworld')
    elseif config.keyboard:check('quit', key) then EngineState:quit()
    elseif config.keyboard:check('up', key) then self:up()
    elseif config.keyboard:check('down', key) then self:down()
    elseif config.keyboard:check('left', key) then self:left()
    elseif config.keyboard:check('right', key) then self:right()
    elseif config.keyboard:check('interact', key) then self:select()
    end

	-- Prevent cursor from going passed the region bounds
    self._highlighted.x = math.clamp(1, self._highlighted.x, #self._regions[1])
    self._highlighted.y = math.clamp(1, self._highlighted.y, #self._regions)
end

function WorldMap:draw()
    local mx, my, mw, mh = self._x, self._y, self._w, self._h
    local maxrx = table.maxn(self._regions[#self._regions])
    local maxry = table.maxn(self._regions)


    for ry, _ in ipairs(self._regions) do
        for rx, regioncolor in ipairs(self._regions[ry]) do
            graphic.rectFillLine(
                regioncolor,
                mx + ((rx - 1) * (mw/maxrx + 1)),
                my + ((ry - 1) * (my- maxry - 5)),
                mw/maxrx,
                mh/maxry,
                5,
                __determineRegionOutline(rx, ry, self._highlighted.x, self._highlighted.y)
            )
        end
    end
end

function WorldMap:up()
    self._highlighted.y = self._highlighted.y - 1
end

function WorldMap:down()
    self._highlighted.y = self._highlighted.y + 1
end

function WorldMap:left()
    self._highlighted.x = self._highlighted.x - 1
end

function WorldMap:right()
    self._highlighted.x = self._highlighted.x + 1
end

function WorldMap:select()
    self.map:warpPlayer(self._highlighted.x-1, self._highlighted.y-1)
    EngineState:load('overworld')
end

return WorldMap
