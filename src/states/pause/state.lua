local path = 'src/states/pause/'

local color = require('color')
local graphic = require('graphic')
local f = math.floor
local r = math.round

local State = class('PauseState', require('src.common.state'))

function State:__init()
  self.camera = require('camera').init()
  self.camera:setCanvas(love.graphics.newCanvas(GAMEWIDTH,GAMEHEIGHT))
end


function State:load()
  self._screens = nil
  self._screens = {
    main = require(path..'screens/main/screen').init(),
    party = nil,
    items = nil,
    settings = nil,
    worldmap = nil,
    saveload = nil,
    bestiary = nil,
  }

  self:loadScreen('main')
end

function State:keypressed(key, scancode, isrepeat)
  local current = self._screens.current

  if config.keyboard:check('pause', key) then gamestate:load('overworld')
  elseif config.keyboard:check('quit', key) then love.event.push('quit')
  elseif config.keyboard:check('up', key) then current:moveSelectionUp()
  elseif config.keyboard:check('down', key) then current:moveSelectionDown()
  elseif config.keyboard:check('left', key) then current:moveSelectionLeft()
  elseif config.keyboard:check('right', key) then current:moveSelectionRight()
  elseif config.keyboard:check('interact', key) then current:choose()
  elseif config.keyboard:check('cancel', key) then current:back()
  else
    nk.keypressed(key, scancode, isrepeat)
  end
end

function State:keyreleased(key, scancode)
  nk.keyreleased(key, scancode)
end

function State:mousemoved(x, y, dx, dy, istouch)
    self._mx = (x / self.camera.scaleX)
    self._my = (y / self.camera.scaleY)
    self._mdx = dx
    self._mdy = y
    nk.mousemoved(x, y, dx, dy, istouch)
  end

function State:mousepressed(x, y, button, istouch)
    self._cx = (x / self.camera.scaleX)
    self._cy = (y / self.camera.scaleY)
    nk.mousepressed(x, y, button, istouch)
  end

function State:mousereleased(x, y, button, istouch)
  nk.mousereleased(x, y, button, istouch)
end

function State:textinput(text)
  nk.textinput(text)
end

function State:wheelmoved(x, y)
  nk.wheelmoved(x, y)
end

function State:loadScreen(screen)
  self._screens.current = self._screens[screen]
  self._screens.current:load()

  print(self._screens.current.name)
end

function State:update(dt)
  local w = love.graphics.getWidth()
  local h = love.graphics.getHeight()
  self.camera:setScale(w/GAMEWIDTH, h/GAMEHEIGHT)
  self._cx = nil
  self._cy = nil
  nk.frameBegin()
  nk.frameEnd()
end

function State:draw()
  EngineState:draw('overworld')

  self.camera:set()
    graphic.text(
      f(self._mx or 0) .. ',' .. f(self._my or 0),
      color.BLACK,
      0,0
    )

    self._screens.current:draw()
  self.camera:unset()
end

return State