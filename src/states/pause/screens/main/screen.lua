local gui = require('gui')
local color = require('color')
local Screen = { name = 'Main' }

Screen.__index = Screen

function Screen.init()
  return setmetatable( {}, Screen)
end

function Screen:load()
  local gw, gh = GAMEWIDTH, GAMEHEIGHT

  local partyOption = {
    text = 'Party'
  }
  local itemsOption = {
    text = 'Items'
  }
  local settingsOption = {
    text = 'Options'
  }
  local exitOption ={
    text = 'Quit'
  }

  self.menu = gui.menu(
    gw - gw / 5, 0,
    gw / 5, (gh / 5) * 4,
    nil,
    color.GRAY,
    {
      partyOption,
      itemsOption,
      settingsOption,
      exitOption
    }
  )
  
  self.menu.selected = 1
end

function Screen:update()
end

function Screen:draw()
  self.menu:draw()
end

function Screen:moveSelectionUp()
end

function Screen:moveSelectionDown()
end

function Screen:moveSelectionLeft()
end

function Screen:moveSelectionRight()
end

function Screen:choose()
end

function Screen:back()
end

return Screen