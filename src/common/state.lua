local State = class('GameState', require('src.common.gameobject'))

function State:load()
end

function State:update()
end

function State:draw()
end

function State:updateDebug()
end

function State:drawDebug()
end

function State:keypressed(key, scandcode, isrepeat)
end

function State:keyreleased(key, scancode)
end

function State:mousemoved(x, y, dx, dy, istouch)
end

function State:mousepressed(x, y, button, istouch)
end

function State:mousereleased(x, y, button, istouch)
end

function State:textinput(text)
end

function State:wheelmoved(x, y)
end

function State:getName()
  return self.__name
end

return State