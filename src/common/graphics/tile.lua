local r = math.round

local Tile = {}
Tile.__index = Tile

local function init(x, y, tw, th, qid)
  return setmetatable(
    {
      qid = qid,
      x = x,
      y = y,
      tw = tw,
      th = th
    },
    Tile)
end

function Tile:draw(atlas, quad, xoffset, yoffset)
  love.graphics.draw(atlas, quad, r(self.x + (xoffset or 0)), r(self.y + (yoffset or 0)))
end

return setmetatable(
  {init=init}
  , {__call = function (_, ...) return init(...) end}
)