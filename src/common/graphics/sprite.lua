local path = 'src/states/overworld/'
local vector = require('vector')

local Sprite = class('Sprite', require('src.common.gameobject'))

local function _printInvalidImg(self, invalidPath)
  if type(invalidPath) ~= type(true) then
    self:error('Image path <'..tostring(invalidPath)..'> is invalid type. Expected <string> received <'..type(invalidPath)..'>')
  end
  return self:cache('retrieve', 'image', 'missing')
end

local function _printMissingAni(self, invalidPath)
  self:error('Animation path <'..tostring(invalidPath)..'> is invalid.' )
  return false
end

local function _drawStatic(self, x, y)
  love.graphics.draw(self.image, x, y)
end

local function _drawAnimation(self, x, y)
  love.graphics.draw(self.image, self.animation.current[self.qindex], x, y, 0, 1, 1)
end

local function _drawFlippedAnimation(self, x, y)
  love.graphics.draw(
    self.image,
    self.animation.current[self.qindex],
    x + (self.animation.current.width or self.animation.defaultwidth),
    y,
    0, -1, 1)
end

local function _determineAnimation(self, dir, action)
  local animation = false

  if type(self.animation[action]) == 'string' then
    animation = self.animation[self.animation[action]]
  elseif type(self.animation[action]) == 'table' then
    animation = self.animation[action]
  end

  if not animation then
    print('Sprite: No action <'..tostring(action)..'> defined')
    return self.animation.actionmissing
  end

  if #animation > 0 then
    return (animation or self.animation[action])
  else
    if dir.y == UP or dir.y == DOWN then
      assert(dir.x == 0, 'Sprite: Direction can not be both horizontal and vertical')
      self.draw = _drawAnimation
      animation = (dir.y == UP and animation.up or animation.down)

    elseif dir.x == LEFT or dir.x == RIGHT then
      assert(dir.y == 0, 'Sprite: Direction can not be both horizontal and vertical')
      self.draw = (dir.x == animation.side.dir and _drawAnimation or _drawFlippedAnimation)
      animation = animation.side

    end
  end
  return animation
end

function Sprite:__init(imgPath, aniPath)
  self:cache('store', 'image', 'missing')
  self.qindex = 1
  self.time = 0
  self.dir = vector()

  if type(imgPath) ~= 'string' then
    self.image = _printInvalidImg(self, imgPath)
    self.draw = _drawStatic
    self.animation = false
    return false
  else
    if not self:cache('store', 'image', imgPath) then
      self.image = _printInvalidImg(self, imgPath)
      self.draw = _drawStatic
      self.animation = false
      return false
    end
    self.image = self:cache('retrieve', 'image', imgPath)
  end

  self.draw = self.draw or (type(aniPath) == 'string' and _drawAnimation or _drawStatic)
  self.animation = (type(aniPath) == 'string' and require(aniPath) or _printMissingAni(self, aniPath))
end

function Sprite:animate(dt, dir, action)
  self.time = self.time + dt

  if self.dir ~= dir
  or self.action ~= action
  then
    self.dir = dir:clone()
    self.action = action
  
    self.animation.current = _determineAnimation(self, dir, action)
    self.quad = self.animation.current[1]
    self.qindex = 1
    self.time = 0
  end

  if not self.animation.current.skip then
    local t = (self.animation.current.time or self.animation.defaulttime)
    if self.time >= t then
      if self.qindex < #self.animation.current then
        self.qindex = self.qindex + 1
      else
        self.qindex = 1
      end
      self.time = 0
    end
  end
end

function Sprite:update(dt, dir, action)
  if self.animation then self:animate(dt, dir, action) end
end

return Sprite