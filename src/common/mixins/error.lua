local ansicolors = require('ansiterm')

local __errors = setmetatable({}, {__mode='k'})
local __printnonfatal = false
local __logging = false

local function __toggleerrorio()
  __printnonfatal = (not __printnonfatal and true or false)
  __logging = (not __logging and true or false)
end

local function __geterrorcount()
  local count = 0
  for _, _class in pairs(__errors) do
    for _, _ in ipairs(_class) do
      count = count + 1
    end
  end
  return count
end

local function __parse(name, message, fatal)
  return (fatal and '(fatal) (' or '(nonfatal) (') .. name .. ') error: ' .. message
end

local function log(object, message, fatal)
end

local function __error(object, message, fatal)
  if not __errors[object] then __errors[object] = {} end

  local __err = __parse(object.__name, message, fatal)
  table.insert(__errors[object],  __err)

  if fatal or __alwaysfatal then
    error(__err)
  elseif fatal == nil and __assumefatal then
    error(__err)
  elseif EngineState:getDebugmode() and __printnonfatal then
    print(
      ansicolors.bright ..
      ansicolors.red ..
      '('..object.__name..') '..
      (fatal and '(fatal)' or '(nonfatal)')..
      ' Error #'..__geterrorcount()..': '..
      ansicolors.reset..
      message
    )
  end

  if __logging then log(object, message, fatal) end
end

return {
  error = __error,
  geterrorcount = __geterrorcount,
  toggleerrorio = __toggleerrorio
}