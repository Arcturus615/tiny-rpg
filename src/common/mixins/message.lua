local ansicolors = require('ansiterm')

local __messages = setmetatable( {}, {__mode='k'} )
local __logging = false
local __printing = false

local function __togglemessageio()
  __printing = (not __printing and true or false)
  __logging = (not __logging and true or false)
end

local function __getmessagecount()
  local count = 0
  for _, _class in pairs(__messages) do
    if k ~= 'events' then
      for _, _ in ipairs(_class) do
        count = count + 1
      end
    end
  end
  return count
end

local function __log(self, message)
end

local function __message(self, message, isevent, eventdata)
  assert(
    type(message) == 'string',
    'Message: (message), expected <string>, got <'..type(message)..'>'
  )

  if isevent then
    assert(eventdata, 'Events require data')
    table.insert(__messages.events, {self.__name, eventdata})
  end

  if not isevent and __printing then
    if not __messages[self] then __messages[self] = {} end
    table.insert(__messages[self], message)
    print( ansicolors.bright .. ansicolors.blue .. '('..self.__name..') ' .. ansicolors.reset.. message)
  end

  if __logging then __log(self, message) end
end

return {
  message = __message,
  getmessagecount = __getmessagecount,
  togglemessageio = __togglemessageio
}