-- TODO: Fix this entire function. This code is currently broken
-- and while it *does* function, it only does so barely and does
-- NOT do what it advertises. Currently, metatables are set
-- incorrectly, and numerous other flaws are prevalent.
-- The comments present throughout exist well after the initial
-- code was put into place, and represent what the functions
-- they refer to *should* accomplish.

local assets = 'src/assets/'
local images = assets..'images/'
local sounds = assets..'sounds/'
local music = assets..'music/'
local scripts = assets..'scripts/' --Not certain about this one

local __errorinvalidtype = 'Invalid data type referenced. Type: '
local __errorinvalidid = 'Invalid id referenced. ID: '
local __errorinvalidfile = 'Invalid id passed. ID: '

-- This set of table definitions creates a set of tables, which will
-- contain references to weak tables once assets are cached.One will be
-- set to derefence tables when all of the to be deferenced tables keys are
-- null, while the other is set to derefence when the value being used
-- as the key is set to null. 
-- 
-- The __usagemap table below is the aforementioned table. The main
-- sub-tables (script, image, etc) will never dereference. The idea
-- here is that you define an asset ID in one of the sub tables
-- as a table itself. You then reference actual objects using the
-- asset within the created table. Once the table is empty, either
-- because all of the objects using the same asset are no longer using it,
-- or if they derefence, the table itself derefernces. 
local __usagemap = {
    script = { [0]=true },
    image =  { [0]=true },
    sound =  { [0]=true },
    music =  { [0]=true }
}

-- The above table ties into this one. This table, again, is set to be a weak
-- table, but using the table keys as what determines whether or not to 
-- dereference, and be garbage collected. This is the table where the actual  
-- storage happens. The asset itself is loaded into memory here, with the 
-- ID for the asset being the table key *in the previous table*. This means
-- that when all objects are no longer making use of an asset, as defined in
-- in the above table, it dereference the asset which is then garbage collected.
local __memorytable={ __mode='k', __newindex = function(...) error(__errorinvalidid.. ...) end }
local __inmemory = {
    -- The __newindex metatable function being set below prevents objects
    -- from accessing values that are not currently in memory. It then throws
    -- an error. There is no reason to actually set this in the above table,
    -- as at no point does this script actually attempt to reference possibly
    -- nonexistent tables in the usage map, as the only interactions with it
    -- are by the objects that are adding or removing themselves from the table
    -- indirectly via the "cache" function that is returned when this script is
    -- included via "require" or "dofile"
    __newindex = function() error(__errorinvalidtype) end,
    
    ['script'] = setmetatable({}, _memorytable),
    ['image'] = setmetatable({}, _memorytable),
    ['sound'] = setmetatable({}, _memorytable),
    ['music'] = setmetatable({}, _memorytable),
  }

-- Ascertain whether or not the asset ID references a valid asset,
-- an if so, call the required love function to load it. Then,
-- return the loaded object to the calling function for storage.
-- if this process failes, throw an error.
local function __storeimg(user, _id)
  if not love.filesystem.getInfo(assets..'images/'.._id..'.png') then
    user:error(__errorinvalidfile..tostring(_id))
    return false
  end
  user:message( 'Caching image '.._id)
  return love.graphics.newImage(images.._id..'.png')
end

local function __storesnd(user, _id) end
local function __storescr(user, _id) end
local function __storemus(user, _id) end

-- The inderect referencing table for object storage functions
local __storefunctions = {
  script = __storescr,
  image = __storeimg,
  music = __storemus,
  sound = __storesnd
}

-- The __store function uses the tricks outlined above to store
-- assets into memory in a dynamic, and easily referenced, way.
local function __store(user, _type, _id)
  -- Ensure the __usagemap has a table with the type defined. If not
  -- throw an error.
  if not type(__usagemap[_type]) == 'table' then 
    user:error(__errorinvalidtype, true)
    return false
  end
  
  -- Ensure that the _id is now referenced in the usage map if it
  -- wasn't already. Further, set that table to be a value weak table.
  if not __usagemap[_type][_id] then
    __usagemap[_type][_id] = setmetatable({}, {__mode='v'})
  end
  
  -- Check whether or not the asset being referenced has been cached
  -- as of yet. If not, cache it.
  if type(__inmemory[_type][ __usagemap[_type][_id] ]) == 'nil' then

    -- Run the storing function with the given parameters. It should be noted
    -- that __storefunction returns a value of false if fails. This is stored,
    -- and must be dereferenced.
    __inmemory[_type][ __usagemap[_type][_id] ] = __storefunctions[_type](user, _id)
    
    -- If the caching process fails, derefernce it, and return false
    if not __inmemory[_type][ __usagemap[_type][_id] ] then
      __inmemory[_type][ __usagemap[_type][_id] ] = nil
      return false
    end
  end

  -- If the table does not already contain a reference to the calling
  -- object, add it.
  if not table.icontains(__usagemap[_type][_id], user) then
    table.insert( __usagemap[_type][_id], user )
  end

  return true
end

local function __retrieve(user, _type, _id)
  return __inmemory[_type][ __usagemap[_type][_id] ] or __inmemory[_type][ __usagemap[_type]['missing'] ]
end

-- Yield the target asset to the cache, removing the object
-- from the usage map.
local function __yield(user, _type, _id)
  user:message('Yielding image '..tostring(_id))
  
  -- Determine whether or not the object ever cached the image
  -- If not, throw an error and return false. If so, set _uindex
  -- to be the numerical index of the objects reference in the
  -- usage map.
  local _uindex = table.icontains( __usagemap[_type][_id], user )
  if _uindex then __usagemap[_type][_id][_uindex] = nil end
  
  -- Run the garbage collector
  collectgarbage()
end

local __calls = {
  -- This __newindex prevents objects from calling nonexistent
  -- functions, throws an error, and returns false.
  __newindex = function (user)
    user:error('Attempted to call undefined caching function')
    return false
  end,
  store = __store,
  yield = __yield,
  retrieve = __retrieve,
}

-- Return just the following function on include, keeping all
-- references to the functions in use local, and inaccessible
return function (user, _which, _type, _id)
  -- Assert that the "user", "_which", "_type" and "_id" are
  -- type "table", "string", "string", and "string"
  assert(type(user) == 'table', 'Expected type <table> got <' .. type(user) .. '>')
  assert(type(_which) == 'string', 'Expected type <string> got <' .. type(_which) .. '>')
  assert(type(_type) == 'string', 'Expected type <string> got <' .. type(_type) .. '>')
  assert(type(_id) == 'string', 'Expected type <string> got <' .. type(_id) .. '>')

  -- Attempt to run the defined caching function, and return
  -- output. 
  return __calls[_which](user, _type, _id)
end
