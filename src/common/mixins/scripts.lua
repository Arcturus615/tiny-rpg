local __scripts = setmetatable( {}, {__mode='k'} )
local _defaultscriptinterval = 0.1
local _scriptdelta = 0

local function __verifyScriptDef(object, _scriptdef)
    --TODO: Stricter vetting of script definitions.
    -- script.tile and script.map need to be tested
    -- and errors need to be thrown if they are too
    -- large to fit in the current map bounds, and
    -- to ensure no negatives are unintentionally used

    local errors = {}
    local name = (_scriptdef.name)
    local overworld = (_scriptdef.overworld or false)
    local map = (_scriptdef.map or false)
    local tile = (_scriptdef.tile or false)
    local script = (_scriptdef or false)

    if not name or not type(name) == 'string' then
        table.insert(errors, 'Invalid script definition [name]. Expected <string>, received <'..type(name)..'>\n')
    end

    if not script or not type(script) == 'function' then
        table.insert(errors, 'Invalid script definition [script]. Expected <function>, received <'..type(script)..'>\n')
    end

    if overworld and not type(overworld) == 'string' then
        table.insert(errors, 'Invalid script definition [overworld]. Expected <string>, received <'..type(overworld)..'>\n')
    end

    if map then
        if not type(map) == 'table' then
            table.insert(errors, 'Invalid script definition [map]. Expected <table>, received <'..type(map)..'>\n')
        else
            if not type(map.x) == 'number' then
                table.insert(errors, 'Invalid script definition [map.x]. Expected <number>, received <'..type(map.x)..'>\n')
            end
            if not type(map.x) == 'number' then
                table.insert(errors, 'Invalid script definition [map.y]. Expected <number>, received <'..type(map.y)..'>\n')
            end
        end
    end

    if tile then
        if not type(tile) == 'table' then
            table.insert(errors, 'Invalid script definition [tile]. Expected <table>, received <'..type(tile)..'>\n')
        else
            if not type(tile.x) == 'number' then
                table.insert(errors, 'Invalid script definition [tile.x]. Expected <number>, received <'..type(tile.x)..'>\n')
            end
            if not type(tile.x) == 'number' then
                table.insert(errors, 'Invalid script definition [tile.y]. Expected <number>, received <'..type(tile.y)..'>\n')
            end
        end
    end

    if #errors > 0 then
        for _error in ipairs(errors) do
            object:error(_error)
        end
        return false
    end
    return true
end

local function __addScript(object, _scriptdef)
    if __verifyScriptDef(object, _scriptdef) then
        if not __scripts[object] then __scripts[object] = {} end
        object:addAttribute('scripted')

        local _scriptedTrigger = {}
        _scriptedTrigger.name = _scriptdef.name
        _scriptedTrigger.overworld = (_scriptdef.overworld or nil)
        _scriptedTrigger.map = (_scriptdef.map or nil)
        _scriptedTrigger.tile = (_scriptdef.tile or nil)
        _scriptedTrigger.flags = (_scriptdef.flags or nil)
        _scriptedTrigger.__interval = (_scriptdef.__interval)
        _scriptedTrigger.__delta = 0
        _scriptedTrigger.__killontrigger = (_scriptdef.__killontrigger or true)
        function _scriptedTrigger.script()
            object:message(
                'Updating script: '
                .. _scriptedTrigger.name
                .. ' attached to '
                .. (object:getID() or object:getName() )
            )
            _scriptdef.script(object)
            if _scriptedTrigger.__killontrigger then
                local _name = _scriptedTrigger.name
                if type(_scriptedTrigger.__killontrigger) == 'number' then
                    _scriptedTrigger.__killontrigger = _scriptedTrigger.__killontrigger - 1
                    if _scriptedTrigger.__killontrigger <= 0 then
                        object:scripts('remove', _name)
                    end
                else
                    object:scripts('remove', _name)
                end
            end
        end

        table.insert(__scripts[object], _scriptedTrigger)

        return true
    end
    return false
end

local function __removeScript(object, _scriptName)
    for i, _scriptdef in ipairs(__scripts[object]) do
        if _scriptdef.name == _scriptName then
            object:message('Removed '.._scriptdef.name..' from '..object:getFullID())
            __scripts[object][i] = nil
        end
    end

    if not __scripts[object] then
        object:removeAttribute('scripted')
        object:message('Scripting for '..object:getFullID()..' has been removed')
    end
end

local function __update(object, dt, map, ...)
    for _, _scriptdef in ipairs(__scripts[object]) do
        if _scriptdef.__delta >= (_scriptdef.__interval or _defaultscriptinterval) then
            _scriptdef.__delta = 0
            local _doupdate = true
            local cmx, cmy = (map:returnCurrentRegion():getRawCoords() or false)
            local ctx, cty = (map:getPlayerLocation() or false)
            local smx, smy = (_scriptdef.map and _scriptdef.map.x or false), (_scriptdef.map and _scriptdef.map.y or false)
            local stx, sty = (_scriptdef.tile and _scriptdef.tile.x or false), (_scriptdef.tile and _scriptdef.tile.y or false)
            local _curr_overworld = (EngineState:getCurrentOverworld() or false)
            local _func_overworld = (_scriptdef.overworld or false)

            if _func_overworld then
                if not _func_overworld.x == _curr_overworld.x
                or not _func_overworld.y == _curr_overworld.y
                then
                    _doupdate = false
                end
            end

            if _scriptdef.map then
                if not cmx == smx or not cmy == smy then
                    _doupdate = false
                end
            end

            if _scriptdef.tile then
                if not ctx == stx or not cty == sty then
                    _doupdate = false
                end
            end

            if _doupdate then
                _scriptdef.script(object, map, ...)
            end
        else
            _scriptdef.__delta = _scriptdef.__delta + dt
        end
    end
end

local function __draw(object)
end

local __commands = {
    add = __addScript,
    remove = __removeScript,
    update = __update,
    draw = __draw
}


return function(object, _command, ...)
    __commands[_command](object, ...)
end
