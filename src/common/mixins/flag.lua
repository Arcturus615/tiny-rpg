local __flags = setmetatable( {}, {__mode='k'} )

local function __setFlag(object, flagName)
    if not __flags[object] then __flags[object] = {} end
    __flags[object][flagName] = true
end

local function __unsetFlag(object)
end

local function __modifyFlagValue(object)
end