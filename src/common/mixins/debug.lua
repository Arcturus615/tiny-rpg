local _d = require('graphic')
local _c = require('color')

local __displaytext = ''
local winw, winh, hudw, hudh
local hudx = 0
local hudy = 0
local offset = 10

local function __draw()
  _d.rectFill( _c.BLACK75, hudx, hudy, hudw, hudh )
  _d.text( __displaytext, _c.WHITE, hudx + offset, hudy + offset, 1000)
end

local function __update()
  __displaytext = ''
  winw, winh = love.graphics.getDimensions()
  hudw = winw / 3.5
  hudh = winh
end

local function __addDebugData(_label, _data)
  __displaytext = __displaytext .. '\n['.._label..': '.._data..']'
end

return {
  draw = __draw,
  update = __update,
  addDebugData = __addDebugData
}