-- Perform defined tests on data sets provided
local _testdir = 'src.assets.tests.'

-- Contain the data that will be used for testing purposes.
-- While in debug mode this data will be always present for
-- hotswapping modes, while in release this will not be available
-- by default and MUST be enabled in the game settings.
local _object_types = {
    gameobject = require(_testdir .. 'gameobject-test'),
    gameaction = require(_testdir .. 'gameaction-test'),
    state = require(_testdir .. 'state-test'),
    entity = require(_testdir .. 'entity-test')
}

local function __run_test(_object, _test_label, _data_set)
    assert(type(_object) == 'table', 'Improper testing object defined. Expected <table> got <'..type(_object)..'>')
    assert(type(_test_label) == 'string', 'Improper testing label defined. Expected <string> got <'..type(_test_label)..'>')
    assert(type(_data_set) == 'table', 'Improper testing data defined. Expected <table> got <'..type(_object)..'>')
    
    -- Run a test on the values provided in _data_set.
    -- Follows the following process:
    -- 1. Object calls object:runTest(testname, data)
    -- 2. runTest then determines what object type "object" is
    -- 3. runTets then runs the test passed, assuming that the 
    --      object type *does* have access to that test.
    
    -- Check if the objects in the objects __types table are
    -- are defined in the _object_types table
    local _type_match

    for k, v in pairs(_object.__types) do
        if type(_object_types[k]) == 'string' then
            _type_match = k
        end
    end

    -- Check if the type match is actually a string. If so, then confirm
    -- the test has been defined, and is a function. If that checks out,
    -- perform the test, and return the output. If any of these checks
    -- fail, return false and an error message. The object can handle the
    -- rest.
    if type(_type_match) == 'string' then
        if type(_object_types[_type_match][_test_label]) == 'function' then
            return _object_types[_type_match][_test_label](_data_set)
        else
            return false, "Malformed test found"
        end
    else
        return false, "Test not found"
    end
end

return {
    runTest = __run_test
}