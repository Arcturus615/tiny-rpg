local Component = class('BaseComponent')

Component.__tostring = function(self) return self.__name end

function Component:__init(entity)
  self.__name = entity.__name
end

function Component:update()
end

function Component:draw()
end

function Component:updateDebug()
end

function Component:drawDebug()
end

function Component:onInteraction()
end

function Component:remove()
end

return Component