local path = 'src/common/components/'
local dialoguePath = 'src/assets/dialogue/'
local Dialogue = class('DialogueComponent', require(path..'component'))

local function __setSpeed(speed)
  Moan.setSpeed = speed or 'medium'
end

local function __parseText(text, otherEntity, speed)
  assert(type(text) ~= nil)
  local variables = {
    PLAYERNAME = otherEntity:getName() or '???',
    OTHERENTITY = otherEntity:getName() or '???',
    TIME = 'FIXME: Time parser',
    DATA = 'FIXME: Date parser',
    SLOW = '',
    MEDIUM = '',
    FAST = ''
  }

  if  text:match( '@SLOW' ) then __setSpeed('slow')
  elseif text:match( '@FAST' ) then __setSpeed('fast')
  elseif text:match( '@MEDIUM' ) then __setSpeed('medium')
  else __setSpeed(speed)
  end

  local __formatted = text:gsub( '%@(%w+)', variables )
  return __formatted
end

function Dialogue:__init(entity)
  if love.filesystem.getInfo(dialoguePath..entity:getID()..'.lua') then
    self.__dialogueTree = require(dialoguePath..entity:getID())
  else
    entity:message('<dialogue> No dialogue file found for '..tostring(entity:getID()).. ' at '..dialoguePath)
    self.__dialogueTree = {'...'}
  end

  self.__defaultSpeed = 'medium'
  entity:addAttribute('interaction')
  local dialogue = self
  function entity:doInteraction(...) return dialogue:onInteraction(...) end
end

function Dialogue:remove(entity)
  entity:removeAttribute('interaction')
  entity.doInteraction = nil
end

function Dialogue:onInteraction(entity, otherEntity)
  EngineState:load('dialogue')
  entity:message('<Dialogue> '..otherEntity:getName() .. ' is attemping to interact with ' .. entity:getName())
  self:parseDialogueTree(entity, otherEntity)
end

function Dialogue:parseDialogueTree(entity, otherEntity)
  local start = self.__resumeIndex or 1
  for i=start, #self.__dialogueTree do
    entity:message('<Dialogue> Starting at index: '..start)
    local message = self.__dialogueTree[i]

    if type(message) == 'string' then
      self:speak(entity, {__parseText(message, otherEntity, self.__defaultSpeed)})
    elseif type(message) == 'table' then
      if message.options then
        self.__resumeIndex = i+1
        self:parseOptions(entity, otherEntity, message, self.__dialogueTree['functions'] or false)
        break
      end
    else
      entity:error('<Dialogue> Unable to parse message table, expected <table>, received <'..type(message)..'>')
      self:speak(entity, {'Error parsing message value, see log for details'})
    end
  end
end

do
  local rungameaction = require('src.common.gameactions')
  function Dialogue:parseOptions(entity, otherEntity, messageTable, storyFunctions)
    local dialogue = self
    local __startMessages = __parseText(messageTable.message, otherEntity, self.__defaultSpeed)
    local __options = {}

    for i, option in ipairs(messageTable.options) do
      local __text = __parseText(option, otherEntity, self.__defaultSpeed)
      local __funcTable = messageTable.functions[i] or false
      local __optionFunc = ''
      local __arguments

      if __funcTable then
        if storyFunctions and __funcTable.type == 'story' then
          if storyFunctions[__funcTable.name] then
            __optionFunc = __funcTable.name
            __arguments = __funcTable.arguments
            local _index = table.icontains(__arguments, 'entity')
            if _index then __arguments[_index] = otherEntity end
          else
            entity:error('<Dialogue> Attempted to store nonexistant function <'..tostring(__funcTable.name)..'>')
          end

        elseif __funcTable.type == 'default' then
          __optionFunc = __funcTable.name
          __arguments = __funcTable.arguments
          local _index = table.icontains(__arguments, 'entity')
          if _index then __arguments[_index] = otherEntity end
        end
      end

      local function __func()
        if __funcTable then
          local _status = (
            __funcTable.type == 'story'
              and storyFunctions(__optionFunc, unpack(__arguments))
              or rungameaction(__optionFunc, __arguments or {})
          )

          if not _status then
            entity:error('<Dialogue> Function <'..__optionFunc..'> with arguments <'..table.print(__arguments)..'> failed.')
          end
        end

        if messageTable.responses then
          if messageTable.responses[i] then
            local __response = __parseText(messageTable.responses[i], otherEntity, self.__defaultSpeed)
            dialogue:speak(entity, {__response})
            dialogue:parseDialogueTree(entity, otherEntity)
          end
        end
      end

      table.insert(__options, {__text, __func})
    end


    self:speak(entity, {__startMessages}, __options)
  end
end

function Dialogue:speak(entity, messages, options, onstart, oncomplete)
    Moan.speak(entity:getName(), messages, {onstart=onstart, oncomplete=oncomplete, options=options} )
end

return Dialogue
