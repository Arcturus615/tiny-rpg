local path = 'src/common/components/'
local hitbox = require('hitbox')
local graphic = require('graphic')
local color = require('color')

local InteractionAbility = class('InteractionComponent', require(path..'component'))

function InteractionAbility:__init(entity)
  self.ibox = hitbox()
  local ability = self

  function entity:interact(otherentity)
    if ability.ibox:checkCollision(otherentity.box) then
      self:message('Interaction with '..otherentity:getName())
      ability:doInteraction(self, otherentity)
      return true
    end
    return false
  end
end

function InteractionAbility:update(dt, entity)
  if entity.dir.x ~= 0 then
    self.ibox.w = entity.box.w / 2
    self.ibox.h = entity.box.h
    self.ibox.y = entity.box.y
    if entity.dir.x > 0 then
      self.ibox.x = entity.box.x + entity.box.w
    else
      self.ibox.x = entity.box.x - self.ibox.w
    end
  end

  if entity.dir.y ~= 0 then
    self.ibox.w = entity.box.w
    self.ibox.h = entity.box.h /2
    self.ibox.x = entity.box.x
    if entity.dir.y > 0 then
      self.ibox.y = entity.box.y + entity.box.h
    else
      self.ibox.y = entity.box.y - self.ibox.h
    end
  end
end

function InteractionAbility:drawDebug(entity, xoffset, yoffset)
  graphic.rectFill(
    color.WHITE50,
    self.ibox.x + (xoffset or 0),
    self.ibox.y + (yoffset or 0),
    self.ibox.w,
    self.ibox.h,
    1
  )
end

function InteractionAbility:doInteraction(entity, otherentity)
  if otherentity:getAttributeValue('interaction') then
    otherentity:doInteraction(otherentity, entity)
  end
end

return InteractionAbility