local __actions = {
  levelUp = function (target)
  end,

  tempModifyStat = function (target)
  end,

  modifyStat = function (stat, rate, target)
    assert( type(stat) == 'string' )
    assert( type(rate) == 'string' or type(rate) == 'number' )

    if target then
      assert( type(target) == 'table', 'Action target, expected type <table> got type <'..type(target)..'>' )
      target:modifyStat(stat, rate)
      return true
    end

    return false
  end
}

return function (_action, _arguments)
  assert(type(_action) == 'string', 'Game action should be type <string>. Got <'..type(_action)..'>')
  assert(type(_action) == 'string', 'Game action arguments should be in a table <table>. Got <'..type(_arguments)..'>')
  return __actions[_action] and __actions[_action](unpack(_arguments)) or false
end