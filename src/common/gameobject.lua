-- Base game object. Used as a combination flyweight
-- and ECS design
local GameObject = class('Object')

-- Set up weak tables for safe object value storage.
-- Setting up things here, and using weak tables, means
-- that once the KEY VALUE, whatever that may be, is
-- set to null, the table dereferences and is garbage
-- collected. As you will see below, the object itself
-- is used as the table key, meaning that once the object
-- owning the sub-table is gone, the sub-table goes with it
local __objectdata = setmetatable( {}, {__mode='k'} )
local __components = setmetatable( {}, {__mode='k'} )
local __attributes = setmetatable( {}, {__mode='k'} )

GameObject.message = require('src.common.mixins.message').message
GameObject.scripts = require('src.common.mixins.scripts')
GameObject.error = require('src.common.mixins.error').error
GameObject.cache = require('src.common.mixins.cache')
GameObject.flag = require('src.common.mixins.flag')
GameObject.addDebugData = require('src.common.mixins.debug').addDebugData
GameObject.runTest = require('src.common.mixins.test').runTest

-- Initialze the various data tables to the object.
function GameObject:__init()
  __attributes[self] = {}
  __components[self] = {}
  __objectdata[self] = {}
end

-- As you will see below, for objects (as in instantiated object)
-- with unique data, you can lock said data using the "_lock" paramter
-- as there is NO way to modify these values outside of these functions,
-- once set the data cannot be unlocked.

-- TODO: Go through these functions and implement a solution to simply
-- setting object.value directly. Likely, what will happen is I will
-- define a secondary table dedicated to locked values, replacing the
-- current method, and set up objects to perform a lookup on that table
-- whenever a new index is attempted to be assigned to the object directly.
function GameObject:setName(value, _lock)
    if not __objectdata[self] then __objectdata[self] = {} end
    if not __objectdata[self].name then __objectdata[self].name = {} end

    if not __objectdata[self].name.lock then
        __objectdata[self].name.string = (value or 'NAMID-1')
    end

    __objectdata[self].name.lock = (_lock or false)
end

function GameObject:setID(value, _lock)
    if not __objectdata[self] then __objectdata[self] = {} end
    if not __objectdata[self].id then __objectdata[self].id = {} end

    if not __objectdata[self].id.lock then
        __objectdata[self].id.string = (value or 'ERRID-1')
    end

    __objectdata[self].id.lock = (_lock or false)
end

function GameObject:getName()
  return (__objectdata[self].name.string or 'NAMID-2')
end

function GameObject:getID()
  return (__objectdata[self].id.string or 'ERRID-2')
end

function GameObject:getFullID()
    return (self:getID()..'::'..self:getName() )
end

-- This function is called every update cycle, and
-- does exactly what it says it does. Simply runs
-- the assigned scripts. This function is terrible
-- and will need to be changed at some point.
-- There should be no "scripted" attribute assigned
-- to the object. Rather, a more robust solution
-- would be to move this check into the script mixin
-- and have *that* code handle this.
function GameObject:updateScripts(dt, map, camera)
  if self:getAttributeValue('scripted') then
    self:scripts('update', dt, map, camera)
  end
end

function GameObject:addAttribute(attribute, value)
    if not __attributes[self][attribute] then
        __attributes[self][attribute] = (value or true)
    end
end

function GameObject:getAttributeValue(attribute)
  return (__attributes[self][attribute] or false)
end

function GameObject:removeAttribute(attribute)
  if __attributes[self][attribute] then
    __attributes[self][attribute] = nil
  end
end

function GameObject:modifyAttribute(attribute, value)
  if not attribute or type(value) == nil then
    self:error(self:getID(), 'Attempted to modify invalid attribute')
    return false
  else
    __attributes[self][attribute] = value
  end
end

function GameObject:addComponent(component)
  table.insert( __components[self], require('src.common.components/'..component)(self) )
  return true
end

function GameObject:updateComponents(dt, camera)
    for _, component in ipairs(__components[self]) do
        component:update(dt, self)
    end
end

-- Check all components assigned to the object, and
-- if a name matchs is found: derefence it.
function GameObject:removeComponent(component_name)
  for i, component in ipairs(__components[self]) do
    if tostring(component) == component_name then
      __components[self][i]:remove(_p[self])
      __components[self][i] = nil
      self:message('Component '..component_name..' has been removed from '.._p[self].id, 1)
      return true
    end
  end

  self:error('Attempted to remove unattached component ('..component_name..')')
  return false
end

return GameObject