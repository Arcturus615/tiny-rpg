local Vector = {}
Vector.__index = Vector
Vector.__type = 'vector'

local function isvector(object)
  assert(object.x and object.y, 'Error: Vector missing required values!')
  return true
end

local function init(x, y)
  return setmetatable({x = (x or 0), y = (y or 0)}, Vector)
end

function Vector:__tostring()
  return math.floor(self.x)..','..math.floor(self.y)
end

function Vector.__add(A, B)
  assert( isvector(A) and isvector(B), 'Error: Expected [vector]' )
  return init(
    A.x + B.x,
    A.y + B.y
  )
end

function Vector.__sub(A, B)
  assert( isvector(A) and isvector(B), 'Error: Expected [vector]' )
  return init(
    B.x - A.x,
    B.y - A.y
  )
end

function Vector.__mul(A, B)
  if type(A) == 'number' then
    return init( (B.x*A), (B.y*A) )
  elseif type(B) == 'number' then
    return init( (A.x*B), (A.y*B) )
  else
    assert( isvector(A) and isvector(B), 'Mul: Wrong argument types. Expected [vector]/[number]')
    return init(
      A.x * B.x,
      A.y * B.y
    )
  end
end

function Vector.__div(A, B)
  assert( isvector(A) and type(B) == 'number',
    'Div: Wrong argument types. Expected [vector]/[number]' )
  return init(
    A.x / B,
    A,y / B
  )
end

function Vector.__eq(A, B)
  assert( isvector(A) and isvector(B), 'Eq: Expected [vector]/[vector]')
  if  A.x == B.x and A.y == B.y then
    return true
  else
    return false
  end  
end

function Vector.distance(A, B)
  assert( isvector(A) and isvector(B), 'Distance: Expected [vector]/[vector]' )
  local x = B.x - A.x
  local y = B.y - A.y
  return math.sqrt(x^2 + y^2)
end

function Vector.normalize(A, B)
  assert( isvector(A) and isvector(B), 'Normalize: Expected [vector]/[vector]')
  local scalar = vector.distance(A, B)
  local d = A - B
  return init(
    d.x/scalar,
    d.y/scalar
  )
end

function Vector:scale(scalar)
  assert( type(scalar) == 'number', 'Scale: Expected [number]')
  self.x = self.x * scalar
  self.y = self.y * scalar
  return self
end

function Vector:clone()
  return init(self.x, self.y)
end

function Vector:getCoordinates()
  return self.x, self.y
end

return setmetatable(
  {init = init, isvector = isvector},
  {__call = function(_, ...) return init(...) end }
)
