local hitbox = require('hitbox')
local clamp = math.clamp
local r = math.round

local Camera = {}
Camera.__index = Camera

Camera._canvas = false
Camera._x = 0
Camera._y = 0
Camera.scaleX = 1
Camera.scaleY = 1

function Camera.init()
  return setmetatable({}, Camera)
end

function Camera:getX()
  return self._x
end

function Camera:getY()
  return self._y
end

function Camera:setCanvas(canvas)
  self._canvas = (canvas or self._canvas)
  self._canvas:setFilter('nearest','nearest')
end

function Camera:drawCanvas(drawFunction)
  love.graphics.setCanvas(self._canvas)
  love.graphics.clear()

  love.graphics.push('all')
  love.graphics.translate(-self._x, -self._y)
  drawFunction()
  love.graphics.pop()
  love.graphics.setCanvas()

  love.graphics.scale(self.scaleX, self.scaleY)
  love.graphics.draw(self._canvas)
end

function Camera:set()
  love.graphics.push('all')
  love.graphics.translate(r(-self._x), r(-self._y))
  love.graphics.scale(self.scaleX, self.scaleY)
end

function Camera:unset()
  love.graphics.pop()
end

function Camera:move(dx, dy)
  self.x = self.x + (dx or 0)
  self.y = self.y + (dy or 0)
end

function Camera:scale(sx, sy)
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function Camera:setScale(sx, sy)
  self.scaleX = (sx or 0)
  self.scaleY = (sy or 0)
end

function Camera:setPosition(x, y)
  if self._bounds then
    self._x = clamp( self._bounds.x1, x, self._bounds.x2 )
    self._y = clamp( self._bounds.y1, y, self._bounds.y2 )
  else
    self._x = x or self._x
    self._y = y or self._y
  end
end

function Camera:getMousePosition()
  return love.mouse.getX() * self.scaleX + self._x, love.mouse.getY() * self.scaleY + self._y
end

function Camera:setBoundary(x1, y1, x2, y2)
  self._bounds = {x1=x1, y1=y1, x2=x2, y2=y2}
end

function Camera:setBoundX1(x)
  if not self._bounds then self:setBoundary(0,0,0,0) end
  self._bounds.x1 = x
end

function Camera:setBoundX2(x)
  if not self._bounds then self:setBoundary(0,0,0,0) end
  self._bounds.x2 = x
end

function Camera:setBoundY1(y)
  if not self._bounds then self:setBoundary(0,0,0,0) end
  self._bounds.y1 = y
end

function Camera:setBoundY2(y)
  if not self._bounds then self:setBoundary(0,0,0,0) end
  self._bounds.y2 = y
end

function Camera:getBoundX1()
  return self._bounds.x1 or false
end

function Camera:getBoundX2()
  return self._bounds.x2 or false
end

function Camera:getBoundY1()
  return self._bounds.y1 or false
end

function Camera:getBoundY2()
  return self._bounds.y2 or false
end

return Camera