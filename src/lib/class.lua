-- A slightly modified copy of the simple oop example found on lua-users.
-- Website is located at: http://lua-users.org/wiki/SimpleLuaClasses
return function (name, base)
  assert(type(name) == 'string', 'Classes must be named.')
  local c = {
    __types = {}
  }
  if type(base) == 'table' then
    for i, v in pairs(base) do
      if i == '__types' then
        table.insert(c.__types, v)
      else
        c[i] = v
      end
    end
    c.__base = base
  end

  c.__index = c
  c.__name = name

  local mt = {}
  function mt.__call(class_tbl, ...)
    local obj = setmetatable({}, c)
    local _b = base
    while _b do
      if _b and _b.__init then _b.__init(obj, ...) end
      _b = _b.__base
    end
    if class_tbl.__init then class_tbl.__init(obj, ...) end
    return obj
  end
  setmetatable(c, mt)
  return c
end