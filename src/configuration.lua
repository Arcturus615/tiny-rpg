local path = 'src/config/'

local Configuration = {}

function Configuration.init()
  return setmetatable(
    {
      keyboard = require(path..'keyboard').init(),
      -- mouse = require(path..'mouse').init,
      -- joystick = require(path..'joystick').init
    },
    Configuration)
end

return Configuration