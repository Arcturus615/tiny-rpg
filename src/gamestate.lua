local path = 'src/states/'
local Pause = require(path..'pause/state')
local Overworld = require(path..'overworld/state')

local Game = class('Game')

function Game:__init(debugmode)
  EngineState = require('src.enginestate'):init(self)

  if debugmode then EngineState:toggleDebugmode() end

  self.debug = {
    update = require('src.common.mixins.debug').update,
    draw = require('src.common.mixins.debug').draw
  }

  self.states = {}
  self.states.current = false
  self.states.debugging = self.debug


  self.message = require('src.common.mixins.message').message
  self.scripts = require('src.common.mixins.scripts')
  self.error = require('src.common.mixins.error').error
  self.cache = require('src.common.mixins.cache')
  self.flag = require('src.common.mixins.flag')
  self.addDebugData = require('src.common.mixins.debug').addDebugData

end

function Game:start()
  self:cache('store', 'image', 'missing')
  self.states.overworld = Overworld(self)
  self.states.pause = Pause(self)
  self:load('overworld')
end

function Game:load(state, ...)
  self:message('('..state..') ' .. (... and tostring(...) or 'No arguments to load'))
  if not self.states[state] then
    self:reload(state, ...)
    return
  end
  
  self.states.current = self.states[state]
  self.states.current:load(...)
  self:message('Switching to state '..tostring(EngineState:getCurrentState()))
end

function Game:reload(state, ...)
  self.states[state] = nil
  self.states[state] = require(path..state..'/state')()
  self:load(state, ...)
end

function Game:getPlayer()
  return self.states.overworld.map:getPlayer()
end

function Game:returnCurrentState()
  return (self.states.current or error())
end

do
  local geterrorcount = require('src.common.mixins.error').geterrorcount
  function Game:update(dt)
    self.states.current:update(dt)

    if EngineState:getDebugmode() then
      self.debug:update()
      self.addDebugData('FPS', love.timer.getFPS())
      self.addDebugData('RAM (Kb)', getvirtualramuse('kb'))
      self.addDebugData('Errors', geterrorcount())
      self.states.current:updateDebug(dt)
    end

    collectgarbage()
  end
end

function Game:draw()
  self.states.current:draw()
  if EngineState:getDebugmode() then
    self.states.current:drawDebug()
    self.debug:draw()
  end
end

function Game:quit()
  self:message('Quitting game.')
  love.event.push('quit')
end

return Game