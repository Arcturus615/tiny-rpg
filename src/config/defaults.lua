return {
  keyboard = {
    up = {'up', 'w'},
    down = {'down', 's'},
    left = {'left', 'a'},
    right = {'right','d'},
    pause = {'p'},
    interact = {'return'},
    cancel = {'x'},
    debug = {'f11'},
    console = {'`'},
    quit = {'escape', 'q'},
    run = {'space'}
  },
  mouse = false,
  joystick = false
}