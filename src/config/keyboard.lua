local path = 'src/config/'

local Config = {}
Config.__index = Config

function Config.init()
  return setmetatable( {keys = require(path..'defaults').keyboard}, Config )
end

function Config:getConfig(keybind)
  return unpack(self.keys[keybind]) or false
end

function Config:setKey(key, keybind)
  if self.keys[keybind] and key then 
    self.keys[keybind] = (key or self.keys[keybind])
  end
end

function Config:resetKey(keybind)
  if self.keys[keybind] then
    self.keys[keybind] = require(path..'defaults').keyboard[keybind]
  end
end

function Config:unsetKey(keybind)
  if self.keys[keybind] then self.keys[keybind] = false end
end

function Config:check(test, subject)
  if not self.keys[test] then return false end
  if type(self.keys[test]) == 'table' then
    for _, setkey in ipairs(self.keys[test]) do
      if subject and subject == setkey then return true end
      if love.keyboard.isDown(setkey) then return true end
    end
  elseif type(self.keys[test] == 'string') then
    if subject and subject == self.keys[test] then return true end
    if love.keyboard.isDown(self.keys[test]) then return true end
  else
    return false
  end
end

return Config