function _G.zpairs(table)
  assert(table[0], 'zpairs: Table must have a 0 index')
  local i = -1
  local m = table.maxn(table)
  return function ()
    i = i + 1
    if i-1 <= m then return i, table[i] end
  end
end

function _G.tern(test, a, b)
  -- From: http://www.frykholm.se/files/markdown.lua
  -- Just a simple ternary function  
  if test then return a else return b end
end

function _G.addlibrarypath(newpath)
  -- add to package paths (if not already present)
  -- Based on: http://lua-users.org/lists/lua-l/2010-07/msg00088.html
  local _gpath = love.filesystem.getRequirePath()
  if not string.find( _gpath, newpath, 0, true) then
    _gpath = string.format("%s/?/init.lua;%s", newpath, _gpath)
    _gpath = string.format("%s/?.lua;%s", newpath, _gpath)
		love.filesystem.setRequirePath(_gpath)
	end
end

function _G.addclibrarypath(newpath)
	-- This is a modification of the previous function that allows the user to
	-- load C libraries. This *cannot* be done from within the `.love` package
	-- and **MUST** be loaded from the filesystem. Hence this function.
  local _OS = love.system.getOS()
  if not string.find(package.cpath, newpath, 0, true) then
    if _OS == 'Linux' or _OS == 'OS X' or _OS == 'Android' then
      package.cpath = string.format("%s/?.so;%s", newpath, package.cpath)
    elseif _OS == 'Windows' then
      package.cpath = string.format("%s/?.dll;%s", newpath, package.cpath)
    end
  end
end

function _G.math.clamp(min, value, max)
	-- If [value] is between [min] and [max], return [value]
	-- otherwise: Return [min] is lesser, or [max] if greater
	return value < min and min or (value > max and max or value)
end

function _G.math.initRandom()
	-- Get around Lua's random bug
	math.randomseed( os.time() )
	math.random() math.random() math.random()
end

function _G.math.round(num) 
    return (num >= 0 and (math.floor(num+ 0.5)) or (math.ceil (num - 0.5)))
end

function _G.table.clone(oldtable)
	-- Quick and dirty table cloning function
	assert(type(oldtable) == 'table', 'Error (table.clone): Expected type <table>, recevied type <'..type(oldtable)..'>')
	local __newtable = {}
	for i, v in pairs(oldtable) do
		__newtable[i] = v
	end
	return __newtable
end

function _G.table.icontains(table, value)
	-- Quick and dirty (and also somewhat broken) function
	-- that checks if a given value is defined by that table
	-- NOTE: This is not recursive (which is good)
	for i, v in ipairs(table) do
		if v == value then return i end
	end
	return false
end

function _G.table.contains(table, key)
	-- Quick function to determine if a [key] is defined
	-- in the specified [table]
	tern(table[key] ~= nil, true, false)
end

function _G.string.split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return unpack(t)
end

function _G.parseparameters(passed, defaults)
	-- Parse the parameters passed and check them against the listed defaults.
	-- NOTE: This function doesn't actually belong here, but will remain for now
	-- eventually, this will need its own file however.
	-- TODO: Remove the harcoded parameters and instead check against another passed value
	assert(type(defaults) == 'table', 'parseparameters: Default parameters must be in a table')
	local params = table.clone(defaults)
	for i, _arg in ipairs(passed) do
		if _arg:match('-d') then params.debug = true end
		if _arg:match('--debug') then params.debug = true end
	end
	return params
end

do
	local _rampasses = {kb=0, mb=1, gb=2}
	function _G.getvirtualramuse(_size)
		-- Determine the current ram usage and output in the specified metric
		-- NOTE: This also probably doesn't belong here. Will probably be rolled into the debug
		-- class at some point.
		assert(type(_size) == type(nil) or _rampasses[_size],
			'getvirtualramuse: String must be kb, mb, or gb. Got: <'..tostring(_size)..'>')
		local _usage = collectgarbage('count')
		local i=0
		while i < (_rampasses[_size] or 0) do
			i=i+1
			_usage = _usage / 1024
		end
		return math.floor(_usage)
	end
end

function _G.table.print(tbl)
  local _s = ''
  for _, v in ipairs(tbl) do
    _s = _s..tostring(v)..' '
  end

  for k, v in pairs(tbl) do
    if not tonumber(k) then _s = _s..tostring(v)..' ' end
  end

  return _s
end

function _G.printcoord(_a, _b)
	-- Convert coordinates into a string
	return '('..tostring(_a)..','..tostring(_b)..')'
end

function _G.table.getlength(_tbl)
    assert(_tbl, 'No table passed to table.getlength')

    -- Iterate through _tbl and count the number of elements
    local i = 0
    for k, v in pairs(_tbl) do
        i = i + 1
    end
    return i
end
