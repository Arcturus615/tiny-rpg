local function __init(self, gamestate, configuration)
	-- Responsible for storing data in regards to the games current state
	-- as well as defines the flow of game logic.
    local _debugMode = false
    local toggleerrorio = require('src.common.mixins.error').toggleerrorio
    local togglemessageio = require('src.common.mixins.message').togglemessageio

    local function __load(self, state, ...)
        gamestate:load(state, ...)
    end

    local function __reload(self, state, ...)
        gamestate:reload(state, ...)
    end

    local function __updateState(self, state)
        if not type(state) == 'string' then
            gamestate:error('Attempt to update improper state. Expected type <string>, received <'..type(state)..'>')
            return false
        elseif not gamestate.states[state] then
            gamestate:error('Attempt to update unloaded state <'..state..'>')
            return false
        end
        gamestate.states[state]:update()
    end

    local function __drawState(self, state)
        if not type(state) == 'string' then
            gamestate:error('Attempt to draw improper state. Expected type <string>, received <'..type(state)..'>')
            return false
        elseif not gamestate.states[state] then
            gamestate:error('Attempt to draw unloaded state <'..state..'>')
            return false
        end
        gamestate.states[state]:draw()
    end

    local function __getCurrentState(self)
        return (gamestate.states.current or false)
    end

    local function __getCurrentOverworld(self)
        return (gamestate.states.overworld or false)
    end

    local function __getCurrentRegion(self)
        return (gamestate.states.overworld:getCurrentRegion() or false)
    end

    local function __getPlayerInfo(self)
        local plr = gamestate:getPlayer()
        local x, y = plr:getPosition()
        local vx, vy = plr:getVelocity()
        local sp = plr:getSpeed()
        local hx, hy, hw, hh = plr:getHitDox()
        local dx, dy = plr:getDirection()

        return x, y, vx, vy, sp, dx, dy, hx, hy, hw, hh
    end

    local function __setTitleFlair(self, flair)
        if type(flair) == 'string' then
            love.window.setTitle(_G.TITLE..' - '..flair)
        else
            gamestate:error('Title flair must be a string!')
        end
    end

    local function __getDebugmode(self)
        return (_debugMode or false)
    end

    local function __setDebugmode(value)
        _debugMode = (type(value) == 'boolean' and value or _debugMode)
    end

    local function __toggleDebugmode(self)
        toggleerrorio()
        togglemessageio()
        if _debugMode then _debugMode = false return end
        _debugMode = true
    end

    local function __quit(self)
        gamestate:quit()
    end

    return {
            updateState = __updateState,
            drawState = __drawState,

            load = __load,
            reload = __reload,
            getCurrentState = __getCurrentState,
            getCurrentRegion = __getCurrentRegion,
            getCurrentOverworld = __getCurrentOverworld,
            getPlayerInfo = __getPlayerInfo,
            getDebugmode = __getDebugmode,

            setTitleFlair = __setTitleFlair,
            setDebugmode = __setDebugmode,
            toggleDebugmode = __toggleDebugmode,

            quit = __quit
        }
end

return {init = __init}
