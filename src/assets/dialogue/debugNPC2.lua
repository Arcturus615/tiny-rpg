return {
  {
    message = 'Hi @PLAYERNAME! How are ya doin~~!?',
    options = {
      'Good!',
      'Eh.',
      'Bad.'
    },
    responses = {
      'Awesome! I\'m so glad to hear that! :D',
      'Oh, well no news is good news~~!',
      'I\'m sorry! I hope things get better! <3'
    },
    functions = {
      [1] = {type='default', name='modifyStat', arguments={'happyness', 5, 'entity'},},
      [3] = {type='default', name='modifyStat', arguments={'happyness', -5, 'entity'},}
    }
  },
  'It was nice to see you!'
}