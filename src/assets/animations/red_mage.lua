local IW, IH = 444, 392
local TW, TH = 16, 16

local function quad(x, y, tw, th)
  return love.graphics.newQuad(
    x,
    y,
    tw or TW,
    th or TH,
    IW,
    IH
  )
end

return{
  defaulttime = 0.5,
  defaultwidth = TW,
  defaultheight = TH,
  run = 'walk',   -- Set which action to use instead
  sleep = { [1] = quad(184, 64), skip = true },
  idle = {
    up = {   [1] = quad(84, 64), skip = true },
    down = { [1] = quad(4, 64),  skip = true },
    side = { [1] = quad(44, 64), skip = true, dir = -1 }, -- This dir key specifies which direction the sprites face,
  },
  walk = {
    up = {   [1] = quad(84, 64), [2] = quad(104, 64), time = 0.25 },
    down = { [1] = quad(24, 64), [2] = quad(4, 64), time = 0.25 },
    side = { [1] = quad(64, 64), [2] = quad(44, 64), time = 0.25, dir = -1 },
    },
  actionmissing = { [1] = quad(424, 64), skip = true}

}